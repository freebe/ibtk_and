package com.ibtk.exercise;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class DetailMenu extends Activity {

	String para;
	Context context;
	VideoView videoView;
	ArrayList<String>list;
	ArrayAdapter<String> adapter;
	ScrollView lv;
	InputMethodManager imm;
	String Intensity, Method, Frequency, URL, Time;
	View view;
	ListView textv;
	static TextView textv2;
	static float size;
	float tmpsize;
	TextView text1,text2,text3,text4;

	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;

	// 드래그시 좌표 저장
	int posX1=0, posX2=0, posY1=0, posY2=0;

	// 핀치시 두좌표간의 거리 저장
	float oldDist = 1f;
	float newDist = 1f;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
		setContentView(R.layout.menu_detail);
		Intent intent = getIntent();
		para = intent.getStringExtra("Param");
		context = getApplicationContext();
		//		Log.e("sdf",para);
		Object obj = JSONValue.parse(para); // 쿼리문 파싱
		JSONObject mainObject = (JSONObject)obj;
		Intensity = "운동 강도 :\n" + (String)mainObject.get("exerciseIntensity").toString() + "\n"; // 목록 배열
		Method = "운동 방법 : \n" + (String)mainObject.get("exerciseMethod").toString() + "\n"; // 목록 배열
		Frequency = "운동 빈도 : \n" + (String)mainObject.get("exerciseFrequency").toString() + "\n"; // 목록 배열
		final String URL = (String)mainObject.get("exerciseVideosUrl").toString(); // 목록 배열
		Time = "운동 시간 : \n" + (String)mainObject.get("exerciseTime").toString() + "\n"; // 목록 배열

		text1 = (TextView)findViewById(R.id.text1);
		text2 = (TextView)findViewById(R.id.text2);
		text3 = (TextView)findViewById(R.id.text3);
		text4 = (TextView)findViewById(R.id.text4);

		ImageButton imgbtn = (ImageButton)findViewById(R.id.videobut);
		imgbtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent1 = new Intent(context, Movie.class);
				intent1.putExtra("Param", URL);
				startActivity(intent1);
			}
		});

		Message message = handler.obtainMessage(3);
		handler.sendMessage(message);						

		lv = (ScrollView) findViewById(R.id.wherelist);
		lv.setOnTouchListener(mTouchEvent);

	}

	final Handler handler = new Handler()
	{
		public void handleMessage(final Message msg)
		{
			switch(msg.what){
			case 1:
				call(1);
				break;
			case 2:
				call(2);
				break;
			case 3:
				text1.setText(Intensity);
				text2.setText(Method);
				text3.setText(Frequency);
				text4.setText(Time);
				text1.setTextSize(10);
				text2.setTextSize(10);
				text3.setTextSize(10);
				text4.setTextSize(10);
				size = 10;
				tmpsize = size;
				break;
			}
		}

		private void call(int arg) {
			if(arg==1){
				tmpsize+=2.0;
			}
			else{
				tmpsize-=2.0;
				if(tmpsize<size) tmpsize = size;
			}
			text1.setTextSize(tmpsize);
			text2.setTextSize(tmpsize);
			text3.setTextSize(tmpsize);
			text4.setTextSize(tmpsize);
		}
	};

	private  OnTouchListener mTouchEvent = new OnTouchListener()
	{
		public boolean onTouch(View v, MotionEvent event)
		{
			int act = event.getAction();
			String strMsg = "";

			switch(act & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:    //첫번째 손가락 터치(드래그 용도)
				posX1 = (int) event.getX();
				posY1 = (int) event.getY();
				mode = DRAG;
				break;
			case MotionEvent.ACTION_MOVE: 
				if(mode == DRAG) {  // 드래그 중
					posX2 = (int) event.getX();
					posY2 = (int) event.getY();

					if(Math.abs(posX2-posX1)>20 || Math.abs(posY2-posY1)>20) {
						posX1 = posX2;
						posY1 = posY2;
						strMsg = "drag";
					}
				} else if (mode == ZOOM) {    // 핀치 중
					newDist = spacing(event);
					if (newDist - oldDist > 20) { // zoom in
						oldDist = newDist;
						Message message = handler.obtainMessage(1);
						handler.sendMessage(message);
					} else if(oldDist - newDist > 20) { // zoom out
						oldDist = newDist;
						Message message = handler.obtainMessage(2);
						handler.sendMessage(message);	       
						strMsg = "zoom out";
					}
				}
				break;
			case MotionEvent.ACTION_UP:    // 첫번째 손가락을 떼었을 경우
			case MotionEvent.ACTION_POINTER_UP:  // 두번째 손가락을 떼었을 경우
				mode = NONE;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:  
				//두번째 손가락 터치(손가락 2개를 인식하였기 때문에 핀치 줌으로 판별)
				mode = ZOOM;

				newDist = spacing(event);
				oldDist = spacing(event);

				break;
			case MotionEvent.ACTION_CANCEL:
			default : 
				break;
			}
			return false;
		}
	};

	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}


}
