package com.ibtk.exercise;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class SubMenu extends Activity implements OnInitListener {

	Button slideHandleButton;
	SlidingDrawer slidingDrawer;
	Context context;
	Button btn[];
	JSONObject content; // content parsing OK;
	JSONArray content_array;
	ImageButton button[] = new ImageButton[6];
	int i;
	private String para="";
	ImageButton butt[] = new ImageButton[6];
	boolean first = true;
	int second = 0;
	int number;
	BufferedReader br;
	private TextToSpeech myTTS;
	private String tmp;
	private ArrayList<String> text;


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		para = intent.getStringExtra("Param");

		setContentView(R.layout.menu_sub);
		butt[0] = (ImageButton)findViewById(R.id.Button01);
		butt[1] = (ImageButton)findViewById(R.id.Button02);
		butt[2] = (ImageButton)findViewById(R.id.Button03);
		butt[3] = (ImageButton)findViewById(R.id.Button04);
		butt[4] = (ImageButton)findViewById(R.id.Button05);
		butt[5] = (ImageButton)findViewById(R.id.Button06);
	}

	private OnTouchListener mTouchEvent = new OnTouchListener()
	{
		public boolean onTouch(View v, MotionEvent event)
		{
			int action=event.getAction();
			int id = v.getId();
			if(action==MotionEvent.ACTION_UP){
				switch (id){
				case (R.id.Button01):
					para="1";
				break;
				case (R.id.Button02):
					para="2";
				break;
				case (R.id.Button03):
					para="3";
				break;
				case (R.id.Button04):
					para="4";
				break;
				case (R.id.Button05):
					para="5";
				break;
				case (R.id.Button06):
					para="6";
				break;
				}
				onStart();
			}
			return true;
		}
	};

	void parsing(final String string){
		new Thread() {
			public void run() {
				HttpURLConnection Connect = null;
				String url = "http://www.ibtk.kr/SeniorExercise/f031e246ee27ca05152b8a09f00f7f21?model_query=";
				String a = "{mainMenuTitle:{\"$regex\":\"";
				String b = "\"}}";

				/*ProgressDialog mProgressDialog = new ProgressDialog(context);//프로그레스 돌림
		        mProgressDialog.show();*/

				try {
					url = url+a+URLEncoder.encode(string, "UTF-8")+b;

					if(checkNetwordState()){
						URL LoginAdd = new URL(url);
						Connect = (HttpURLConnection) LoginAdd.openConnection();
						Connect.setDoOutput(true);
						Connect.setInstanceFollowRedirects(false);
						Connect.connect();
						Connect.getResponseMessage();


						if (Connect.getResponseCode() == HttpURLConnection.HTTP_OK){
							//check = true;

							br = new BufferedReader(new InputStreamReader(Connect.getInputStream()));
							StringBuilder sb = new StringBuilder();
							String line;
							while ((line = br.readLine()) != null) {
								sb.append(line+"\n");
							}
							line = sb.toString();

							br.close();

							Object obj = JSONValue.parse(line); // 쿼리문 파싱
							JSONObject mainObject = (JSONObject)obj;
							content_array = (JSONArray)mainObject.get("content"); // 목록 배열

							number = Integer.parseInt(mainObject.get("numberOfElements").toString()); // 목록 갯수
							btn = new Button[number];
							for(int i=0; i<number; i++){	                
								content = (JSONObject)content_array.get(i); // i번째 배열 
								Message message = handler.obtainMessage(1,i,number,content.get("subMenuTitle"));
								handler.sendMessage(message);						
							}
						}
					}
					else{
						handler.sendEmptyMessage(3);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	final Handler handler = new Handler()
	{
		public void handleMessage(final Message msg)
		{
			LinearLayout linear1 = (LinearLayout) findViewById(R.id.LinearLayout01);
			switch (msg.what) {
			case 1:				
				btn[msg.arg1] = new Button(context);
				btn[msg.arg1].setText("" + msg.obj.toString());
				//btn[msg.arg1].setWidth(66);
				btn[msg.arg1].setTextSize(25);
				btn[msg.arg1].setId(msg.arg1);
				text.add(msg.obj.toString());
				btn[msg.arg1].setTextColor(Color.rgb(0, 0, 0));
				linear1.addView(btn[msg.arg1]);
				btn[msg.arg1].setOnClickListener(new Button.OnClickListener( ) {
					Intent intent;
					public void onClick(View v) {
						switch (v.getId()){
						case 0:
							intent = new Intent(context, DetailMenu.class);
							content = (JSONObject)content_array.get(0);
							intent.putExtra("Param", content.toString());
							onInit(0);
							startActivity(intent);
							break;

						case 1:
							Intent intent = new Intent(context, DetailMenu.class);
							content = (JSONObject)content_array.get(1);
							intent.putExtra("Param", content.toString());
							onInit(1);
							startActivity(intent);
							break;

						case 2:
							intent = new Intent(context, DetailMenu.class);
							content = (JSONObject)content_array.get(2);
							intent.putExtra("Param", content.toString());
							onInit(2);
							startActivity(intent);
							break;

						case 3:
							intent = new Intent(context, DetailMenu.class);
							content = (JSONObject)content_array.get(3);
							intent.putExtra("Param", content.toString());
							onInit(3);
							startActivity(intent);
							break;
						case 4:
							intent = new Intent(context, DetailMenu.class);
							content = (JSONObject)content_array.get(4);
							intent.putExtra("Param", content.toString());
							onInit(4);
							startActivity(intent);
							break;
						case 5:
							intent = new Intent(context, DetailMenu.class);
							content = (JSONObject)content_array.get(5);
							intent.putExtra("Param", content.toString());
							onInit(5);
							startActivity(intent);
							break;
						}
					}
				});
				if(msg.arg1 == msg.arg2 -1) {
					first = false;
				}
				//btn[msg.arg1].setOnTouchListener(mListen);
				break;

			case 2:
				/*if(msg.arg1 == 1){//x번을 클릭 함
					Log.e("sdf","sdf11");
					button[msg.arg1] = (ImageButton)findViewById(R.id.Button01);
					button[msg.arg1].setOnClickListener(new Button.OnClickListener(){
		                public void onClick(View v){
		                	onRestart();
		                }
		            });

				}*/

				break;
			case 3:
				Toast.makeText(context, "인터넷 상태를 확인 해 주세요.", Toast.LENGTH_LONG).show();

			case 4:
				Toast.makeText(context, "자료를 가져올 수 없습니다.", Toast.LENGTH_LONG).show();
			}
		}
	};


	public void onStart(){
		super.onStart();

		text = new ArrayList<String>();

		if(!first) { 
			first = true;
			ViewGroup layout = (ViewGroup) findViewById(R.id.LinearLayout01);

			for(int j=0; j<number; j++){
				View command = layout.findViewById(j);			
				layout.removeView(command);
			}
		}

		slideHandleButton = (Button) findViewById(R.id.slideHandleButton);
		slidingDrawer = (SlidingDrawer) findViewById(R.id.SlidingDrawer);
		context = getApplicationContext();


		for(int i=0; i<6; i++){
			butt[i].setOnTouchListener(mTouchEvent);
		}

		slidingDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {

			@Override
			public void onDrawerOpened() {
				slideHandleButton.setBackgroundResource(R.drawable.openarrow);
			}
		});

		slidingDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {
			@Override
			public void onDrawerClosed() {
				slideHandleButton.setBackgroundResource(R.drawable.closearrow);
			}
		});
		butt[0].setBackgroundResource(R.drawable.traffic2);
		butt[1].setBackgroundResource(R.drawable.muscle);
		butt[2].setBackgroundResource(R.drawable.heart);
		butt[3].setBackgroundResource(R.drawable.strech);
		butt[4].setBackgroundResource(R.drawable.dancer);
		butt[5].setBackgroundResource(R.drawable.end);

		switch(para){
		case "1":
			butt[0].setBackgroundResource(R.drawable.traffic);
			parsing("준비");
			break;
		case "2":
			butt[1].setBackgroundResource(R.drawable.muscle2);
			parsing("근력");
			break;
		case "3":
			butt[2].setBackgroundResource(R.drawable.heart2);
			parsing("심폐지구력");
			break;
		case "4":
			butt[3].setBackgroundResource(R.drawable.strech2);
			parsing("유연성");
			break;
		case "5":
			butt[4].setBackgroundResource(R.drawable.dancer2);
			parsing("평행성");
			break;
		case "6":
			butt[5].setBackgroundResource(R.drawable.end2);
			parsing("정리");
			break;			
		}
		myTTS = new TextToSpeech(context, this);
	}

	private boolean checkNetwordState() {
		ConnectivityManager connManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo state_3g = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo state_wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		return state_3g.isConnected() || state_wifi.isConnected();
	}

	@Override
	public void onInit(int arg0) {
		myTTS.setLanguage(Locale.KOREA);
		if(para == "1"){
			myTTS.speak("준비 운동", TextToSpeech.QUEUE_ADD, null);
		}
		else if(para == "2"){
			myTTS.speak("근력 강화", TextToSpeech.QUEUE_ADD, null);
		}
		else if(para == "3"){
			myTTS.speak("신폐 지구력 강화", TextToSpeech.QUEUE_ADD, null);
		}
		else if(para == "4"){
			myTTS.speak("유연성 강화", TextToSpeech.QUEUE_ADD, null);
		}
		else if(para == "5"){
			myTTS.speak("평행성 / 체력 강화", TextToSpeech.QUEUE_ADD, null);
		}
		else if(para == "6"){
			myTTS.speak("정리 운동", TextToSpeech.QUEUE_ADD, null);
		}
		if(!first) myTTS.speak(text.get(arg0),TextToSpeech.QUEUE_ADD, null);
	}
	protected void onDestroy() {
		super.onDestroy();
		//myTTS.shutdown();
	}	
}