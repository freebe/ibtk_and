package com.ibtk.exercise;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainMenu extends Activity implements OnInitListener{
	
	private ImageButton img1;
	private ImageButton img2;
	private ImageButton img3;
	private ImageButton img4;
	private ImageButton img5;
	private ImageButton img6;
	TextToSpeech myTTS;
	
	protected void onResume(){
		super.onResume();
		img1.setBackgroundResource(R.drawable.bg11);
		
		img2.setBackgroundResource(R.drawable.bg12);
		
		img3.setBackgroundResource(R.drawable.bg13);
		
		img4.setBackgroundResource(R.drawable.bg14);
		
		img5.setBackgroundResource(R.drawable.bg15);
		
		img6.setBackgroundResource(R.drawable.bg16);
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
		setContentView(R.layout.menu_main);
		img1 = (ImageButton) findViewById(R.id.btn1);
		TextView tv1 = (TextView)findViewById(R.id.tv1);
		img1.setBackgroundResource(R.drawable.bg11);
		img2 = (ImageButton) findViewById(R.id.btn2);
		img2.setBackgroundResource(R.drawable.bg12);
		img3 = (ImageButton) findViewById(R.id.btn3);
		img3.setBackgroundResource(R.drawable.heart);
		img4 = (ImageButton) findViewById(R.id.btn4);
		img4.setBackgroundResource(R.drawable.strech);
		img5 = (ImageButton) findViewById(R.id.btn5);
		img5.setBackgroundResource(R.drawable.dancer);
		img6 = (ImageButton) findViewById(R.id.btn6);
		img6.setBackgroundResource(R.drawable.end);
		
		TextView tv2 = (TextView)findViewById(R.id.tv2);
		TextView tv3 = (TextView)findViewById(R.id.tv3);
		TextView tv4 = (TextView)findViewById(R.id.tv4);
		TextView tv5 = (TextView)findViewById(R.id.tv5);
		TextView tv6 = (TextView)findViewById(R.id.tv6);
		
		img1.setOnTouchListener(mTouchEvent);
		tv1.setOnTouchListener(mTouchEvent);
		img2.setOnTouchListener(mTouchEvent);
		tv2.setOnTouchListener(mTouchEvent);
		img3.setOnTouchListener(mTouchEvent);
		tv3.setOnTouchListener(mTouchEvent);
		img4.setOnTouchListener(mTouchEvent);
		tv4.setOnTouchListener(mTouchEvent);
		img5.setOnTouchListener(mTouchEvent);
		tv5.setOnTouchListener(mTouchEvent);
		img6.setOnTouchListener(mTouchEvent);
		tv6.setOnTouchListener(mTouchEvent);
		
		myTTS = new TextToSpeech(this, this);
	}
	
	private  OnTouchListener mTouchEvent = new OnTouchListener()
    {
        public boolean onTouch(View v, MotionEvent event)
        {
            
            //Button b=(Button)v;
            
            int action=event.getAction();
            int id = v.getId();
            
           if(action==MotionEvent.ACTION_DOWN){
        	   if(id==R.id.btn1 || id==R.id.tv1){
        		   img1.setBackgroundResource(R.drawable.bg21);
        		   onInit(1);
        	   }else if(id==R.id.btn2 || id==R.id.tv2){
        		   img2.setBackgroundResource(R.drawable.bg22);
        		   onInit(2);
               }else if(id==R.id.btn3 || id==R.id.tv3){
            	   img3.setBackgroundResource(R.drawable.bg23);
            	   onInit(3);
               }else if(id==R.id.btn4 || id==R.id.tv4){
            	   img4.setBackgroundResource(R.drawable.bg24);
            	   onInit(4);
               }else if(id==R.id.btn5 || id==R.id.tv5){
            	   img5.setBackgroundResource(R.drawable.bg25);
            	   onInit(5);
               }else{
            	   img6.setBackgroundResource(R.drawable.bg26);
            	   onInit(6);
               }        	   
            }
           if(action==MotionEvent.ACTION_UP){
        	   if(id==R.id.btn1 || id==R.id.tv1){
        		   Intent intent1 = new Intent(MainMenu.this, SubMenu.class);
        		   intent1.putExtra("Param", "1");
        		   startActivity(intent1);
        	   }else if(id==R.id.btn2 || id==R.id.tv2){
        		   Intent intent1 = new Intent(MainMenu.this, SubMenu.class);
        		   intent1.putExtra("Param", "2");
        		   startActivity(intent1);
               }else if(id==R.id.btn3 || id==R.id.tv3){
            	   Intent intent1 = new Intent(MainMenu.this, SubMenu.class);
        		   intent1.putExtra("Param", "3");
              	   startActivity(intent1);
               }else if(id==R.id.btn4 || id==R.id.tv4){
            	   Intent intent1 = new Intent(MainMenu.this, SubMenu.class);
        		   intent1.putExtra("Param", "4");
              	   startActivity(intent1);
               }else if(id==R.id.btn5 || id==R.id.tv5){
            	   Intent intent1 = new Intent(MainMenu.this, SubMenu.class);
        		   intent1.putExtra("Param", "5");
              	   startActivity(intent1);
               }else{
            	   Intent intent1 = new Intent(MainMenu.this, SubMenu.class);
        		   intent1.putExtra("Param", "6");
              	   startActivity(intent1);
               }        	   
            }
            return true;
        }
    };

	@Override
	public void onInit(int status) {
		myTTS.setLanguage(Locale.KOREA);
		switch (status){
			case 1: myTTS.speak("준비 운동", TextToSpeech.QUEUE_ADD, null);
		 break;
			case 2: myTTS.speak("근력 강화", TextToSpeech.QUEUE_ADD, null);
			break;
			case 3: myTTS.speak("심폐 지구력 강화", TextToSpeech.QUEUE_ADD, null);
			break;
			case 4: myTTS.speak("유연성 강화", TextToSpeech.QUEUE_ADD, null);
			break;
			case 5: myTTS.speak("평행성 / 체력 강화", TextToSpeech.QUEUE_ADD, null);
			break;
			case 6: myTTS.speak("정리 운동", TextToSpeech.QUEUE_ADD, null);
			break;
		
		}
	}
	
}
