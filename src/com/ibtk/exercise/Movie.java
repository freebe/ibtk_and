package com.ibtk.exercise;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class Movie extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		String para = intent.getStringExtra("Param");
		setContentView(R.layout.vi);

		VideoView videoView = (VideoView) findViewById(R.id.videoView1);

		MediaController mediaController = new MediaController(this);

		mediaController.setAnchorView(videoView);

		Uri video = Uri.parse(para);

		videoView.setMediaController(mediaController);

		videoView.setVideoURI(video);

		videoView.requestFocus();

		videoView.start();

	}
}