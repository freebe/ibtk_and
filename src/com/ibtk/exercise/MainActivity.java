package com.ibtk.exercise;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
		setContentView(R.layout.activity_main);
		blink();
	}
	
	void blink(){
		 final Handler handler = new Handler();
		    new Thread(new Runnable() {
		        @Override
		        public void run() {
		        int timeToBlink = 1000;    //in milissegunds
		        try{Thread.sleep(timeToBlink);}catch (Exception e) {}
		            handler.post(new Runnable() {
		                @Override
		                    public void run() {
		                    ImageView blinkImg = (ImageView)findViewById(R.id.start);
		                    if(blinkImg.getVisibility() == View.VISIBLE){
		                    	blinkImg.setVisibility(View.INVISIBLE);
		                    }else{
		                    	blinkImg.setVisibility(View.VISIBLE);
		                    }
		                    blink();
		                }
		                });
		            }
		        }).start();
	}
	
	 public boolean onTouchEvent(MotionEvent event) {
         super.onTouchEvent(event);
        
         if(event.getAction()== MotionEvent.ACTION_DOWN) {
        	 Intent intetn1 = new Intent(this, MainMenu.class);
        	 startActivity(intetn1);
        	 finish();
         }
         return true;
         }
}
